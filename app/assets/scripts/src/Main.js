ST.core.App = {
	settings: {},
	Utils: ST.core.Util,
	encryption: '',
	alphabet: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
	Tween: new ST.core.Util.Tween(),

	init: function() {
		this.binds();
	},

	binds: function() {
		// Navbar
		$('.navbar .menu-button').on('click', this.handlerMenu.bind(this));
		$('.menu .menu__item').on('click', this.handlerMenuItem.bind(this));
		$('.home .buttons-list__item').on('click', this.handlerSelect.bind(this));
		$('.logo').on('click', this.goToHome.bind(this));

		// Common
		$('.back').on('click', this.goToHome.bind(this));

		// Crypto
		$('.encryption .buttons-list__item.encode').on('click', this.handlerEncode.bind(this));
		$('.encryption .buttons-list__item.decode').on('click', this.handlerDecode.bind(this));
		$('.key-box .buttons-list__item').on('click', this.handlerConfirmKey.bind(this))
	},

	handlerMenu: function(event) {
		var target = $(event.currentTarget);
		var sidebar = $('.sidebar');

		if (sidebar.hasClass('visible')) {
			target.removeClass('active');
			sidebar.removeClass('visible');
		} else {
			target.addClass('active');
			sidebar.addClass('visible');
		}
	},

	handlerMenuItem: function(event) {
		var target = $(event.currentTarget);
		var title = target.text();

		$('.page-title').text(title);

		if (target.hasClass('start')) {
			this.goToHome();
		} else if (target.hasClass('des')) {
			this.encryption = 1;
			this.goToEncryption();
		} else {
			this.encryption = 2;
			this.goToEncryption();
		}
	},

	handlerSelect: function(event) {
		var target = $(event.currentTarget);
		var index = target.attr('data-index');
		var title = target.text();

		$('.page-title').text(title);
		
		switch (index) {
			case '1':
				this.encryption = 1;
				break;
			case '2':
				this.encryption = 2;
				break;
		}

		this.goToEncryption();
	},

	loading: function() {
		$('.cover').addClass('visible');
		$('.menu-button').removeClass('active');
		$('.sidebar').removeClass('visible');
		$('.textarea').val('');

		setTimeout(function() {
			$('.cover').removeClass('visible');
		}, 1700);
	},

	goToHome() {
		this.loading();

		$('.encryption').fadeOut(function() {
			$('.home').fadeIn('fast');
		});
	},

	goToEncryption() {
		this.loading();

		$('.home').fadeOut(function() {
			$('.encryption').fadeIn('fast');
		});
	},

	handlerEncode: function() {
		switch (this.encryption) {
			case 1:
				this.desEncryption();
				break;
			case 2:
				this.openKeyBox('encode');
				break;
		}
	},

	handlerDecode: function() {
		switch (this.encryption) {
			case 1:
				this.desDecryption();
				break;
			case 2:
				this.openKeyBox('decode');
				break;
		}
	},

	openKeyBox: function(type) {
		$('body').addClass('hidden-scroll');
		$('.key-box').attr('data-type', type).fadeIn();
	},

	handlerConfirmKey: function() {
		var box = $('.key-box');
		var key = $('.key-box .select').find(':selected').text().toLowerCase();
		var type = box.attr('data-type');

		box.fadeOut();

		if (type == 'encode') {
			this.cesarEncryption(key);
		} else {
			this.cesarDecryption(key);
		}

		box.find('.select option:selected').attr('selected', false);
	},

	desEncryption: function() {
		var text = $('.text-encode').val().toLowerCase();
		var value = '';

		for (var i = 0; i < text.length; i++) {
			var binary = this.textToBinary(text.charAt(i));
			var parts = this.divideString(binary);
			var xor = this.binaryToXor(parts.part1, parts.part2);
			var partFinal = xor + parts.part1;
			var inverseBinary = this.inverseBinary(2, 5, partFinal);

			if (i == text.length-1) {
				value = $('.text-decode').val() + inverseBinary;
			} else {
				value = $('.text-decode').val() + inverseBinary + '9';
			}

			$('.text-encode').val('');
			$('.text-decode').val(value);
		}
	},

	desDecryption: function() {
		var text = $('.text-decode').val();
		var words = text.split('9');
		var value = '';

		for (var i = 0; i < words.length; i++) {
			var inverseBinary = this.inverseBinary(2, 5, words[i], -1);
			var parts = this.divideString(inverseBinary, -1);
			var xnor = this.binaryToXnor(parts.part1, parts.part2);
			var p2Final = this.inverseBinary(1, 0, xnor, -1);
			var partFinal = parts.part2 + p2Final;
			var binaryToText = this.binaryToText(partFinal);

			value += binaryToText;
		}

		$('.text-decode').val('');
		$('.text-encode').val(value.replace(/A/g, ' '));
	},

	cesarEncryption: function(key) {
		var index = this.alphabet.indexOf(key);
		var arr2 = this.reorderArray(index);
		var text = $('.text-encode').val().toLowerCase();
		var textFinal = '';

		for (var i = 0; i < text.length; i++) {
			var letter = '';

			if (text.charAt(i) == ' ') {
				letter = ' ';
			} else {
				 letter = arr2[this.alphabet.indexOf(text.charAt(i))];
			}
			
			textFinal += letter;
		}

		$('.text-encode').val('');
		$('.text-decode').val(textFinal);
	},

	cesarDecryption: function(key) {
		var index = this.alphabet.indexOf(key);
		var arr2 = this.reorderArray(index);
		var text = $('.text-decode').val().toLowerCase();
		var textFinal = '';

		for (var i = 0; i < text.length; i++) {
			var letter = '';

			if (text.charAt(i) == ' ') {
				letter = ' ';
			} else {
				letter = this.alphabet[arr2.indexOf(text.charAt(i))];
			}

			textFinal += letter;
		}

		$('.text-decode').val('');
		$('.text-encode').val(textFinal);
	},

	textToBinary(text) {
		var binary = '';
		
		for (var i = 0; i < text.length; i++) {
			binary += text.charCodeAt(i).toString(2);
		}

		return binary;
	},

	binaryToText: function(text) {
		return String.fromCharCode(parseInt(text, 2));
	},

	divideString: function(str, neg) {
		var part1 = '';
		var part2 = '';
		var x = '';

		if (!neg) {
			if (!str % 2 == 0) {
				str = '0' + str;
			}
		}

		for (var i = 0; i < str.length; i++) {
			if (i < str.length / 2) {
				part1 += str.charAt(i);
			} else {
				part2 += str.charAt(i);
			}
		}

		var partes = {
			part1: part1,
			part2: part2
		}

		return partes;
	},

	binaryToXor: function(part1, part2) {
		var xor = '';

		for (var i = 0; i < part1.length; i++) {
			if (part1.charAt(i) == part2.charAt(i)) {
				xor += '0';
			} else {
				xor += '1';
			}
		}

		return xor;
	},

	binaryToXnor: function(part1, part2) {
		var xnor = '';
		
		for (var i = 0; i < part1.length; i++) {
			if (part1.charAt(i) != part2.charAt(i)) {
				xnor += '0';
			} else {
				xnor += '1';
			}
		}

		return xnor;
	},

	binaryToAscii: function(str) {
		return parseInt(str, 2);
	},

	asciiToString: function(ascii) {
		return String.fromCharCode(ascii);
	},

	inverseBinary: function(key1, key2, str, type) {
		var r = '';

		for (var i = 0; i < str.length; i++) {
			if (type != -1) {
				if (str.charAt(i) == '0') {
					r += key1;
				} else {
					r += key2;
				}
			} else {
				if (str.charAt(i) == key1)	 {
					r += '0';
				} else {
					r += '1';
				}
			}			
		}

		return r;
	},

	reorderArray: function(index) {
		return this.alphabet.slice(index).concat(this.alphabet.slice(0, index));
	}
};

$(document).ready(function() {
	ST.core.App.init();
});