ST.core.Util = {
  loadImage: function(url, callback) {
    var image = new Image();
    image.onload = function() {
      if (callback) {
        callback();
      }
    };
    image.src = url;
  },

  randomIntFromInterval: function(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  },

  getQueryParam: function(param) {
    var result = location.search.match(new RegExp("(\\?|&)" + param + "(\\[\\])?=([^&]*)"));

    return result ? result[3] : false;
  },

  getImagePath: function(picture) {
    return ST.i.config.serverUrl + ST.i.config.serverImagePath + picture;
  },

  validateEmail: function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  },

  request: function(obj) {
		jQuery.ajax({
			url: obj.url,
			type: obj.type,
			dataType: 'json',
			processData: false,
			contentType: obj.contentType ? obj.contentType : 'application/json; charset=utf-8',
			data: JSON.stringify(obj.data),
		})
		.done(function(data) {
			if (obj.callback) obj.callback(data);
		})
		.fail(function(data) {
			console.log('ERROR:', data);
		})
		.always(function() {
			//console.log('Complete');
		});
	},

  Render: function() {
    var thisObjR = this;
    this.requestAnimationID = 0;
    this.registered = [];

    this.register = function( params ) {
      //console.log('[ RENDER ] REGISTER');
      var newParams = params;
      newParams.active = ( params.id === undefined ) ? true : params.active;
      newParams.id = ( params.id === undefined ) ? thisObj.createUniqueID() : params.id;

      this.registered.push( newParams );

      if ( this.registered.length === 1 )
      this.update();

      return new RenderObj( params );
    }.bind(this);
    this.kill = function( id ) {
      //console.log('[ RENDER ] KILL');
      for ( var a = 0; a < this.registered.length; a++ ) {
        if ( this.registered[a].id === id )
        this.registered.splice(a,1);
      }

      if ( this.registered.length === 0 )
      cancelAnimationFrame( this.requestAnimationID );
    }.bind(this);
    this.update = function() {
      //console.log('[ RENDER ] UPDATING');
      if ( thisObjR.registered.length === 0 ) return;

      var a = 0,
      infoObj;

      for ( ; a < thisObjR.registered.length; a++ ) {
        infoObj = thisObjR.registered[a];
        if ( infoObj.active )
        infoObj.update();
      }

      thisObjR.requestAnimationID = requestAnimationFrame( thisObjR.update );
    };
    this.getObjByID = function( id ) {
      if (id!==undefined) {
        for ( var a = 0; a < this.registered.length; a++ ) {
          if ( this.registered[a].id === id )
          return this.registered[a];
        }
      }
      return false;
    };

    //CLASS OBJETCT RESPONSE
    function RenderObj( params ) {
      return function( params ) {
        this.kill = function() {
          //console.log("[ RENDER - " + this.id + " ] KILL");
          //this.active = false;
          thisObjR.kill( this.id );
        }.bind(this);
        this.suspend = function() {
          //console.log("[ RENDER - " + this.id + " ] SUSPEND");
          this.active = false;
        }.bind(this);
        this.resume = function(newUpdateFunction) {
          //console.log("[ RENDER - " + this.id + " ] RESUME");
          if ( newUpdateFunction !== undefined )
          this.update = newUpdateFunction;
          this.active = true;
        }.bind(this);

        return this;
      }.call( params );
    }
  },
  Interpolation: function( interpolation, min, max ) {
    var range = max - min,
    piece = (Math.max(min, Math.min(max, interpolation)) - min) / range;
    return piece;
  },
  Tween: function() {
    var thisObj = ST.core.Util;

    this.getEasing = function( data, language ) {
      var a, values, easing,
      tweenDefault = ( language === "css" ) ? ["cubic", "ease-out"] : ["linear"],
      temp = ( data !== undefined ) ? data.split('.') : tweenDefault,
      ease = temp[1],
      tween = temp[0];

      easing = (tween === "linear") ? thisObj.Easing[ tween ] : thisObj.Easing[ tween ][ ease ];

      return easing;
    };
    this.get = function ( props ) {
      var count = 0, calculation, now, delta, interpolation,
      durationSecs = props.timing,
      onUpdate = props.update,
      onComplete = props.complete,
      last = new Date().getTime(),
      easefunction = ( props.easeF !== undefined ) ? thisObj.getEasing(props.easeF, 'js') : thisObj.Easing.linear;

      function interpolate() {
        now = new Date().getTime();
        delta = Math.min(((now - last) / 1000), durationSecs);
        calculation = easefunction( delta, props.start, props.end - props.start, durationSecs );
        interpolation = thisObj.Interpolation( calculation, props.start, props.end - props.start );

        if ( onUpdate !== undefined )
        onUpdate( calculation, interpolation );

        if ( delta >= durationSecs ) {
          if ( onComplete !== undefined )
          onComplete( props.end );
        } else
        requestAnimationFrame(interpolate);
      }

      interpolate();
    };
  },
  //EASING FUNCTIONS
  Easing: {
    //R.Penner easing t=time, b=start, c=delta, d=duration
    linear: function (t, b, c, d) { return c*t/d + b; },
    Quad: {
      easeIn: function(t, b, c, d) { return c*(t/=d)*t*t*t + b; },
      easeOut: function(t, b, c, d) { return -c * ((t=t/d-1)*t*t*t - 1) + b; },
      easeInOut: function(t, b, c, d) { if ((t/=d/2) < 1) return c/2*t*t*t*t + b; return -c/2 * ((t-=2)*t*t*t - 2) + b; }
    },
    Quart: {
      easeIn: function (t, b, c, d) { return c*(t/=d)*t*t*t + b; },
      easeOut: function (t, b, c, d) { return -c * ((t=t/d-1)*t*t*t - 1) + b; },
      easeInOut: function (t, b, c, d) { if ((t/=d/2) < 1) return c/2*t*t*t*t + b; return -c/2 * ((t-=2)*t*t*t - 2) + b; }
    },
    Quint: {
      easeIn: function (t, b, c, d) { return c*(t/=d)*t*t*t*t + b; },
      easeOut: function (t, b, c, d) { return c*((t=t/d-1)*t*t*t*t + 1) + b; },
      easeInOut: function (t, b, c, d) { if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b; return c/2*((t-=2)*t*t*t*t + 2) + b; }
    },
    Cubic: {
      easeIn: function (t, b, c, d) { t /= d; return c*t*t*t + b; },
      easeOut: function (t, b, c, d) { t /= d; t--; return c*(t*t*t + 1) + b; },
      easeInOut: function (t, b, c, d) { t /= d/2; if (t < 1) return c/2*t*t*t + b; t -= 2; return c/2*(t*t*t + 2) + b; }
    },
    Back: {
      easeIn: function (t, b, c, d, s) { if (s === undefined) s = 1.70158; return c*(t/=d)*t*((s+1)*t - s) + b;},
      easeOut: function (t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
      },
      easeInOut: function (t, b, c, d, s) { if (s === undefined) s = 1.70158; if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b; return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b; }
    },
    Elastic: {
      easeIn: function (t, b, c, d) { var s=1.70158;var p=0;var a=c; if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3; if (a < Math.abs(c)) { a=c; var s=p/4; } else var s = p/(2*Math.PI) * Math.asin (c/a); return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b; },
      easeOut: function (t, b, c, d) { var s=1.70158;var p=0;var a=c; if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3; if (a < Math.abs(c)) { a=c; var s=p/4; } else var s = p/(2*Math.PI) * Math.asin (c/a); return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b; },
      easeInOut: function (t, b, c, d) { var s=1.70158;var p=0;var a=c; if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5); if (a < Math.abs(c)) { a=c; var s=p/4; } else var s = p/(2*Math.PI) * Math.asin (c/a); if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b; return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b; }
    }
  }
};