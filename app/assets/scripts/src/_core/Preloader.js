ST.core.Preloader = {

    init: function(images, callback) {
        this.element = jQuery('.preloader');
        this.app = jQuery('#app');
        this.progress = this.element.find('.progress-percent');
        this.circle = $('#svg #bar');
        this.percent = this.element.find('.percent');
        this.cache = jQuery('.cache');

        this.circle.hide();

        this.images = images || [];
        this.imagesLoaded = 0;

        if (this.images.length) {
            this.load(this.images, callback);
        }
    },

    show: function(real) {
        // this.percent.html('0%');
        // this.element.fadeIn();
        var _this = this,
            real = real || false;

        if (!real) {
            // setTimeout(function() {
                this.element.addClass('fake');
            // }, 500);
        } else {
            this.element.removeClass('fake');
        }

        this.element.addClass('visible');

        this.progress.html('0%');
        var r = this.circle.attr('r');
        var c = Math.PI*(r*2);
        var pct = ((100-0)/100)*c;

        this.circle.css({ strokeDashoffset: pct });
    },

    hide: function(real) {
        // this.element.fadeOut();
        var real = real || false;

        if (!real) {
            this.element.addClass('loaded');
            var _this = this;
            setTimeout(function() {
                _this.element.removeClass('visible');
            }, 400);
        } else {
            // this.element.removeClass('visible');
            this.app.addClass('visible');
            // this.element.fadeOut(100);
        }
    },

    load: function(images, callback) {
        this.show(true);

        var _this = this,
        imageObjs = [];

        setTimeout(function() {
            _this.circle.show();

            if (images.length) {
                _this.images = images;
                _this.imagesLoaded = 0;

                for (var i = 0; i < _this.images.length; i++) {
                    imageObjs[i] = new Image();
                    imageObjs[i].onload = function() {
                        _this.imagesLoaded++;
                        _this.update(callback, true);
                    };
                    imageObjs[i].src = _this.images[i];
                    _this.cache.append(imageObjs[i]);
                }
            } else {
                _this.update(callback, true);
            }

        }, 200);
    },

    update: function(callback, real) {
        var percent = Math.round((100 / this.images.length) * this.imagesLoaded);

        percent = parseInt(percent);

        if (isNaN(percent)) {
            percent = 100; 
        } else {
            var r = this.circle.attr('r');
            var c = Math.PI*(r*2);

            if (percent < 0) { percent = 0;}
            if (percent > 100) { percent = 100;}

            var pct = c - ((percent/100)*c);
            // var pct = c - ((percent / 100) * c);

            // console.log(c);
            // console.log(pct);

            this.circle.css({ strokeDashoffset: pct});
            this.progress.html(parseInt(percent) + '%');
        }
        // console.log(percent + '%');

        // if (percent > 99) percent = 99;s

        // this.percent.html(percent + '%');

        if (this.imagesLoaded === this.images.length) {
            if (callback) {
                setTimeout(function() {
                    callback.apply();
                }, 1000);
            }

            this.hide(real);
        }
    }

};